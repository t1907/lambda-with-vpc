resource "aws_cloudwatch_log_group" "yada" {
  name = "/aws/lambda/calculator"
  tags = {
    Environment = "test"
    Application = "serviceA"
  }
}