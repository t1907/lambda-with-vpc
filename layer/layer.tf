data "archive_file" "zip" {
  output_path = "${path.module}/../awesome_layer.zip"
  type        = "zip"
  source_dir  = "${path.module}/../src/"
}

resource "aws_lambda_layer_version" "later" {
  layer_name          = "awesome-layer-for-calc"
  filename            = data.archive_file.zip.output_path
  source_code_hash    = data.archive_file.zip.output_base64sha256
  compatible_runtimes = ["python3.9"]
}

output "layer_arn" {
  value = aws_lambda_layer_version.later.arn
}