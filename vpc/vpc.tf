
data "aws_vpc" "vpc" {
  default = true
}

data "aws_subnet_ids" "example" {
  vpc_id = data.aws_vpc.vpc.id
}

resource "aws_security_group" "http_https_sg" {
  name   = "https-http-sg"
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    Name = "https-http-sg"
  }
}

resource "aws_security_group_rule" "ingress_22" {
  description       = "TLS from VPC"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.http_https_sg.id
  type              = "ingress"

}

resource "aws_security_group_rule" "ingress_80" {
  description       = "TLS from VPC"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.http_https_sg.id
  type              = "ingress"
}

resource "aws_security_group_rule" "egress_443" {
  description       = "TLS from VPC"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.http_https_sg.id
  type              = "egress"
}