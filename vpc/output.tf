output "vpc_info" {
  value = {
    vpc_id = data.aws_vpc.vpc.id
    subnets= data.aws_subnet_ids.example.ids
    security_group = aws_security_group.http_https_sg.id
  }
}