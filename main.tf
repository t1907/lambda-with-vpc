provider "aws" {
  region = "us-east-1"
}

module "layer" {
  source = "./layer"
}

module "cloudwatch" {
  source = "./cw"
}

module "iam" {
  source       = "./iam"
  cw_group_arn = module.cloudwatch.cloudwatch_group
}

module "vpc" {
  source = "./vpc"
}

data "archive_file" "zip_lambda" {
  output_path = "calculator.zip"
  type        = "zip"
  source_file  = "./src/lambda/calculator.py"
}

resource "aws_lambda_function" "lambda" {
  function_name = "calculator"
  role = module.iam.lambda_role
  handler = "calculator.lambda_handler"
  description = "this is sample calculator"
  filename = data.archive_file.zip_lambda.output_path
  runtime = "python3.9"
  layers = [module.layer.layer_arn]

  environment {
    variables = {
      foo = "bar"
    }
  }
  vpc_config {
    security_group_ids = [module.vpc.vpc_info["security_group"]]
    subnet_ids = module.vpc.vpc_info["subnets"]
  }
  depends_on = [module.layer.layer_arn]
}

output "vpc" {
  value = module.vpc.vpc_info
}

/*
vpc
subnets
security_groups
iam_role





*/